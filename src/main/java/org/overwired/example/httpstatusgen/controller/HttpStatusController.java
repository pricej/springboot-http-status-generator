package org.overwired.example.httpstatusgen.controller;

import static java.lang.String.valueOf;

import brave.Tracer;
import brave.propagation.TraceContext;
import org.overwired.example.httpstatusgen.domain.HttpStatusGenResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Produce standard output using a different HTTP Status Code for each endpoint.
 */
@RequestMapping("/")
@Controller
public class HttpStatusController {

    private static final Logger log = LoggerFactory.getLogger(HttpStatusController.class);

    private final Tracer tracer;

    @Autowired
    public HttpStatusController(Tracer tracer) {
        this.tracer = tracer;
    }

    @GetMapping("{status}")
    public ResponseEntity<HttpStatusGenResponse> httpStatus(@PathVariable int status,
                                                            @RequestHeader HttpHeaders headers) {
        log.info("calling httpStatus({})", status);
        logHeaders(headers);
        TraceContext traceContext = tracer.currentSpan().context();
        return new ResponseEntity<>(httpStatusGenResponse(headers, traceContext), HttpStatus.resolve(status));
    }

    private HttpStatusGenResponse httpStatusGenResponse(HttpHeaders headers, TraceContext traceContext) {
        return new HttpStatusGenResponse(valueOf(traceContext.spanId()),
                                         valueOf(traceContext.spanId()),
                                         headers);
    }

    private void logHeaders(@RequestHeader HttpHeaders headers) {
        final Set<Map.Entry<String, List<String>>> entries = headers.entrySet();
        log.info("  headers:");
        for (Map.Entry<String, List<String>> entry : entries) {
            log.info("    {}: {}", entry.getKey(), entry.getValue());
        }
    }

}
