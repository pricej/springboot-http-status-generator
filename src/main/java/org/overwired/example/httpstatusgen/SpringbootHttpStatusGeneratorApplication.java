package org.overwired.example.httpstatusgen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootHttpStatusGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootHttpStatusGeneratorApplication.class, args);
	}
}
