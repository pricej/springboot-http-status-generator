package org.overwired.example.httpstatusgen.domain;

import org.springframework.http.HttpHeaders;

/**
 * Represents an HttpStatusGen response.
 */
public class HttpStatusGenResponse {
    private final String traceId;
    private final String spanId;
    private final HttpHeaders headers;

    public HttpStatusGenResponse(String traceId, String spanId, HttpHeaders headers) {
        this.traceId = traceId;
        this.spanId = spanId;
        this.headers = headers;
    }

    public String getTraceId() {
        return traceId;
    }

    public String getSpanId() {
        return spanId;
    }

    public HttpHeaders getHeaders() {
        return headers;
    }

}
