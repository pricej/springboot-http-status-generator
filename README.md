# springboot-http-status-generator

A simple Spring Boot web project used to generate HTTP status codes to enable experimenting with Spring Boot RestTemplate error handling in another client.